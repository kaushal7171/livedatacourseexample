package com.example.livedatasimpleexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView mTv;
    private Button mBtn;
    MyLiveDataModel myLiveDataModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTv=findViewById(R.id.tv);
        mBtn=findViewById(R.id.button);

    myLiveDataModel=new ViewModelProvider(this).get(MyLiveDataModel.class);

        LiveData<Integer> count=myLiveDataModel.getInitialData();
        count.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                mTv.setText(String.valueOf(integer));
            }
        });
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myLiveDataModel.getData();
            }
        });

    }
}