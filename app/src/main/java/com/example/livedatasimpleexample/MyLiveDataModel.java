package com.example.livedatasimpleexample;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyLiveDataModel extends ViewModel {
    int count=0;
    MutableLiveData<Integer> mutableLiveData=new MutableLiveData<>();

    public MutableLiveData<Integer> getInitialData(){
        mutableLiveData.setValue(count);
        return  mutableLiveData;
    }
    public void getData(){
        count+=1;
        mutableLiveData.setValue(count);
    }
}
